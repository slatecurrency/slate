<center>

SLATE Core integration/staging tree
===================================

</center>

<p align="center">
    <a href="https://slate.io/">
    <img src="src/qt/res/src/slate.svg"
        height="130"></a>
</p>
<p align="center">
    <a href="https://discord.gg/ENuwHH5">
        <img src="https://img.shields.io/discord/308323056592486420.svg?logo=discord"
            alt="chat on Discord"></a>
    <a href="https://twitter.com/intent/follow?screen_name=slatecurrency">
        <img src="https://img.shields.io/twitter/follow/slatecurrency.svg?style=social&logo=twitter"
            alt="follow on Twitter"></a>
    <a href="https://twitter.com/intent/tweet?text=Wow:&url=https%3A%2F%2Fgithub.com%2Fslatecurrency%2Fslate">
        <img src="https://img.shields.io/twitter/url/https/github.com/slatecurrency/slate.svg?style=social"
            alt="Tweet"></a>
    <a href="https://github.com/slatecurrency/slate/watchers">
        <img src="https://img.shields.io/github/watchers/slatecurrency/slate.svg?style=social&label=Watch"
            alt="GitHub watchers"></a>
    <a href="https://github.com/slatecurrency/slate/followers" alt="GitHub followers">
        <img src="https://img.shields.io/github/followers/slatecurrency.svg?style=social&label=Follow" /></a>
</p>
<p align="center">
    <a href="https://badge.fury.io/gh/slatecurrency%2Fslate">
        <img src="https://badge.fury.io/gh/slatecurrency%2Fslate.svg"
            alt="GitHub versslate"></a>
    <a href="https://travis-ci.org/slatecurrency/slate">
        <img src="https://travis-ci.org/slatecurrency/slate.svg?branch=master"
            alt="Build status"></a>
    <a href="https://build.snapcraft.io/user/slateapps/slate">
        <img src="https://build.snapcraft.io/badge/slateapps/slate.svg"
            alt="Snap Status"></a>
    <a href="https://github.com/slatecurrency/slate">
        <img src="https://img.shields.io/github/license/slatecurrency/slate.svg"
            alt="GitHub license"></a>
    <a href="https://github.com/slatecurrency/slate/compare/0.1.03...master" alt="commits to be deployed">
        <img src="https://img.shields.io/github/commits-since/slatecurrency/slate/0.1.03.svg?label=commits%20to%20be%20deployed" /></a>
    <a href="https://github.com/slatecurrency/slate/network">
        <img src="https://img.shields.io/github/forks/slatecurrency/slate.svg"
            alt="GitHub forks"></a>
    <a href="https://github.com/slatecurrency/slate/stargazers">
        <img src="https://img.shields.io/github/stars/slatecurrency/slate.svg"
            alt="GitHub stars"></a>
</p>


<center>

<p align="center">
    <a href="https://github.com/slatecurrency/slate/releases/latest">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/latest/total.svg"
            alt="Latest Release"></a>
    <a href="https://github.com/slatecurrency/slate/releases/download/v0.1.03/slate-0.1.03-osx-unsigned.dmg">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/v0.1.03/slate-0.1.03-osx-unsigned.dmg.svg"
            alt="Download Slate Setup for Windows 32-bit"></a>
    <a href="https://github.com/slatecurrency/slate/releases/download/v0.1.03/slate-0.1.03-osx64.tar.gz.exe">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/v0.1.03/slate-0.1.03-osx64.tar.gz.svg"
            alt="Download Slate Setup for Windows 64-bit"></a>
    <a href="https://github.com/slatecurrency/slate/releases/download/v0.1.03/slate-0.1.03-win32-setup-unsigned.exe">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/latest/slate-0.1.03-win32-setup-unsigned.exe.svg"
            alt="Download Slate Setup for Windows 32-bit"></a>
    <a href="https://github.com/slatecurrency/slate/releases/download/v0.1.03/slate-0.1.03-win64-setup-unsigned.exe">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/latest/slate-0.1.03-win64-setup-unsigned.exe.svg"
            alt="Download Slate Setup for Windows 64-bit"></a>
    <a href="https://github.com/slatecurrency/slate/releases/download/v0.1.03/slate-0.1.03-win32.zip">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/latest/slate-0.1.03-win32.zip.svg"
            alt="Download Slate Setup for Windows 32-bit"></a>
    <a href="https://github.com/slatecurrency/slate/releases/download/v0.1.03/slate-0.1.03-win64.exe">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/latest/slate-0.1.03-win64.svg"
            alt="Download Slate Setup for Windows 64-bit"></a>
    <a href="https://github.com/slatecurrency/slate/releases/download/v0.1.03/slate-0.1.03-i686-pc-linux-gnu.tar.gz">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/v0.1.03/slate-0.1.03-i686-pc-linux-gnu.tar.gz.svg"
            alt="Download Slate Setup for Windows 32-bit"></a>
    <a href="https://github.com/slatecurrency/slate/releases/download/v0.1.03/slate-0.1.03-x86_64-linux-gnu.tar.gz">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/v0.1.03/slate-0.1.03-x86_64-linux-gnu.tar.gz.svg"
            alt="Download Slate Setup for Windows 64-bit"></a>
    <a href="https://github.com/slatecurrency/slate/releases/download/v0.1.03/slate-0.1.03-arm-linux-gnueabihf.tar.gz">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/v0.1.03/slate-0.1.03-arm-linux-gnueabihf.tar.gz.svg"
            alt="Download Slate Setup for Windows 32-bit"></a>
    <a href="https://github.com/slatecurrency/slate/releases/download/v0.1.03/slate-0.1.03-aarch64-linux-gnu.tar.gz">
        <img src="https://img.shields.io/github/downloads/slatecurrency/slate/v0.1.03/slate-0.1.03-aarch64-linux-gnu.tar.gz.svg"
            alt="Download Slate Setup for Windows 64-bit"></a>
</p>


What is SLATE?
--------------


![SLATE](https://slate.io/img/b1-icons.png)

SLATE is a blockchain-based entertainment utility protocol powered by a
cryptographically secure multilayered network. Decentralized delivery yields
low-cost, high-speed, high-definition media access globally. Consumers will be
able to spend SLATE cryptocurrency (SLX) on some of the best entertainment the
world has to offer. Tickets will be forgery resistant, virtually eliminating
fraud. Service providers holding SLX can earn even more by storing and
delivering content.

![applayer](https://slate.io/img/b11-img1.png)

Together with this use case SLATE also features fast and private transactions
with low transaction fees and a low environmental footprint.  It utilizes a
custom Proof of Stake protocol for securing its network and uses an innovative
variable seesaw reward mechanism that dynamically balances 90% of its block
reward size between masternodes and staking nodes and 10% dedicated for budget
proposals.

Homepage: [https://slate.io](https://slate.io)

### Masternode Network

The SLATE network consists of a primary network, the distributed ledger for the SLX cryptocurrency, and a secondary layer of “masternodes.” These service nodes store videos and deliver them to Binge customers. Masternodes also quickly secure the generation and redemption of forgery-resistant tokenized tickets for SLATIX. The genius of the network is in the aligned incentives: the SLX cryptocurrency motivates service providers to maintain an always-on global network.


### Binge introduces BVOD™ (Blockchain Video on Demand)

The first premium decentralized streaming platform to utilize the power of distributed ledger technology. For the Binge streaming media platform, blockchain technology facilitates streaming content and transparent behavioral analytics that reveal consumer tastes. Binge’s model of transparency is an entertainment-industry first. Binge will provide real-time revenue sharing between content providers and creators, while mediating access to the valuable network data and analysis that improves content development. 


### SLATIX

<img src="http://slatix.com/logo.png" height="130" alt="SLATIX">

[Learn more about SLATIX](https://slatix.com/)

Powered by the SLATE network, SLATIX will be the new go-to mobile app for entertainment discovery and ticketing. Consumers using SLATIX can discover great content through a curated system of authenticated reviews, and earn substantial discounts and access to entertainment events, including movies, concerts, plays, museums and sporting events. Repeat users will enjoy loyalty rewards and an unparalleled entertainment experience. Curation of a trustworthy review system is incentivized with Slate token rewards (SLX). Since the tickets are secondary tokens on the Slate blockchain, they are forgery-resistant and securely exchangeable on the secondary marketplace.

### Tokenized Ticketing

[SLATIX](https://slatix.com/) will use blockchain technology to create low-cost tokenized ticketing systems with compelling features. SLATIX’s deep analytics will be designed to make ticketing on the blockchain a better way for venues to connect with consumers directly and reward their loyalty. SLATIX tickets will be forgery resistant, virtually eliminating fraud. Small and medium enterprise can benefit from SLATIX’s low setup and operating costs, while the user-friendly app is intended to make DIY ticket distribution and redemption a snap.

### Industry-Leading Open Analytics

Meaningful data is key to sound business decisions. Slate’s powerful analytic tools are designed to make market discovery easy by revealing local, regional, and global viewership patterns. Industry movers will gain a new creative edge when producing content. Detailed demographics help match likely consumers with content they love. Slate Analytics will offer unprecedented access to relevant network data. Creators can take control by accessing the database directly and customizing analysis. Slate’s Open Analytics aims to set a new industry standard.

### Unrestricted Global Access

We believe that consumers are the only authorities that matter when it comes to content decisions. Not governments. Not companies. The blockchain gives power to the people. Binge’s decentralized distribution system is unstoppable, safeguarding consumer choice. Slate cryptocurrency will eliminate barriers to international commerce since the blockchain mediates payment to content creators and license-holders. Trustless transactions mean fast payments that bypass banks and bureaucracy, while still honoring the licensing agreements. Accessible content. Fair payment. The road to freedom is being paved by the blockchain.

### Slate Team

The Slate team consists of talented business, creative, and technical professionals with expertise in multiple facets of entertainment and technology. SEG officers, directors, advisors and agents bring extensive experience in the development of scalable businesses from concept to commercialization; the design and implementation of blockchain technology; the execution of token sales; and the production, acquisition and worldwide distribution of feature films, series and documentaries.


License
-------

SLATE Core is released under the terms of the MIT license. See [COPYING](COPYING) for more
information or see https://opensource.org/licenses/MIT.

Development Process
-------------------

The `master` branch is regularly built and tested, but is not guaranteed to be
completely stable. [Tags](https://github.com/slatecurrency/slate/tags) are created
regularly to indicate new official, stable release versions of SLATE Core.

The contribution workflow is described in [CONTRIBUTING.md](CONTRIBUTING.md).

</center>
